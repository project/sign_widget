import { View, LabeledFieldView, createLabeledInputText, Template, ButtonView, submitHandler, ListView, ListItemView} from 'ckeditor5/src/ui';
import {icons} from 'ckeditor5/src/core';

/**
 * A class rendering the information required from user input.
 *
 * @extends module:ui/view~View
 *
 * @internal
 */
export default class SignatureView extends View {

  /**
   * @inheritdoc
   */
  constructor(editor) {
    const locale = editor.locale;
    super(locale);

    // Add cdn signature.
    this.config = editor.config.get('signature');
    let cdn = this.config.cdn;
    if (this.config.local && cdn.length) {
      let link = document.createElement("link");
      link.rel = "stylesheet";
      link.type = "text/css";
      link.href = cdn;
      document.head.appendChild(link);
    }
    //Create canvas for drawing.
    this.imageCanvasView = this._createCanvas('Signature');
    this.penColorInputView = this._createInput(editor.t('Color'), 'color');
    this.penColorInputView.fieldView.value = this.config.penColor;
    this.sizeInputView = this._createInput(editor.t('Dot size'), 'range');
    this.sizeInputView.fieldView.value = this.config.dotSize;

    // Create the save and cancel buttons.
    this.saveButtonView = this._createButton(
      'Save', icons.check, 'ck-button-save'
    );
    this.saveButtonView.type = 'submit';

    this.cancelButtonView = this._createButton(
      'Cancel', icons.cancel, 'ck-button-cancel'
    );
    // Delegate ButtonView#execute to FormView#cancel.
    this.cancelButtonView.delegate('execute').to(this, 'cancel');
    let collection = [
      this.saveButtonView,
      this.cancelButtonView
    ];
    if (this.config.signTool) {
      collection.unshift(this.sizeInputView);
      collection.unshift(this.penColorInputView);
    }
    collection.unshift(this.imageCanvasView);
    this.childViews = this.createCollection(collection);


    this.setTemplate({
      tag: 'form',
      attributes: {
        class: ['ck', 'ck-signature-form', 'ck-responsive-form'],
        tabindex: '-1'
      },
      children: this.childViews
    });

  }

  /**
   * @inheritdoc
   */
  render() {
    super.render();

    // Submit the form when the user clicked the save button or
    // pressed enter the input.
    submitHandler({
      view: this
    });
  }

  // Create a generic input field.
  _createCanvas(label) {
    const canvas = new View();
    canvas.setTemplate({
      tag: 'div',
      children: [{
        tag: 'canvas',
        attributes: {
          class: ['ck', 'signature'],
          tabindex: '-1'
        },
      }]
    });
    return canvas;
  }

  // Create a generic input field.
  _createInput(label, type = 'text') {
    const labeledInput = new LabeledFieldView(this.locale, createLabeledInputText);
    if (type != 'hidden') {
      labeledInput.label = Drupal.t(label);
    }
    if (type != 'text') {
      labeledInput.fieldView.inputMode = type;
      let tmp = labeledInput.fieldView.template;
      tmp.attributes.type = type;
      labeledInput.fieldView.setTemplate(new Template(tmp));
    }
    if (type == 'range') {
      labeledInput.fieldView.inputMode = type;
      let tmp = labeledInput.fieldView.template;
      tmp.attributes.type = type;
      tmp.attributes.min = 2.5;
      tmp.attributes.max = 7;
      tmp.attributes.step = 0.1;
      labeledInput.fieldView.setTemplate(new Template(tmp));
    }
    return labeledInput;
  }

  // Create a generic button.
  _createButton(label, icon, className) {
    const button = new ButtonView();

    button.set({
      label,
      icon,
      tooltip: true,
      class: className
    });

    return button;
  }

}
