import { Plugin } from 'ckeditor5/src/core';
import { Widget } from 'ckeditor5/src/widget';
import InsertSignatureCommand from "./InsertSignatureCommand";

// cSpell:ignore UrlParser InsertSignatureCommand
export default class SignatureEditing extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();
    this._defineCommands();
  }

  _defineSchema() {
    // Schemas are registered via the central `editor` object.
    const schema = this.editor.model.schema;

    schema.register('signature', {
      // Behaves like a self-contained object (e.g. an image).
      isObject: true,
      // Allow in places where other blocks are allowed (e.g. directly in the root).
      allowWhere: '$text',
      isInline: true,
      allowAttributes: ['class'],
    });

    schema.register('svgIconImage', {
      allowIn: 'signature',
      allowAttributes: [ 'src', 'class'],
    });

  }

  /**
   * Converters determine how CKEditor 5 models are converted into markup and
   * vice-versa.
   */
  _defineConverters() {
    // Converters are registered via the central editor object.
    const { conversion } = this.editor;
    // Data Downcast Converters: converts stored model data into HTML.
    // These trigger when content is saved.
    //
    // Instances of <urlParser> are saved as
    // <div class="simple-box">{{inner content}}</div>.
    conversion.for('downcast').elementToElement({
      model: 'signature',
      view: {
        name: 'span',
        classes: 'signature',
      },
    });

    // Upcast Converters: determine how existing HTML is interpreted by the
    // editor. These trigger when an editor instance loads.
    //
    // If <div class="signature"> is present in the existing markup
    // processed by CKEditor, then CKEditor recognizes and loads it as a
    // <urlParser> model.
    conversion.for('upcast').elementToElement({
      model: 'signature',
      view: {
        name: 'span',
        classes: 'signature',
      },
    });


    //
    // SvgImage > SVG (<svg>)
    //
    const svgSignImage = {
      model: 'svgSignImage',
      view: 'img',
    };
    conversion.for('upcast').elementToElement(svgSignImage);
    conversion.for('downcast').elementToElement(svgSignImage);

  }

  _defineCommands() {
    const editor = this.editor;
    editor.commands.add(
      'InsertSignatureCommand',
      new InsertSignatureCommand(editor),
    );
  }
}
