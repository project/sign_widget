/**
 * @file registers the Signature toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView, ContextualBalloon, clickOutsideHandler } from 'ckeditor5/src/ui';
import FormView from './SignatureView';
import icon from '../../../../icons/sign.svg';

export default class SignatureUI extends Plugin {
  init() {
    const editor = this.editor;
    this._balloon = this.editor.plugins.get(ContextualBalloon);
    this.formView = this._createFormView();

    // This will register the SignatureUI toolbar button.
    editor.ui.componentFactory.add('signature', (locale) => {
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Signature'),
        icon: icon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
      const command = editor.commands.get('InsertSignatureCommand');
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () => {
        this._showUI();
        // Load canvas signature.
        const canvas = document.querySelector("canvas.signature");
        if(canvas){
          // Process on change color or size.
          this.signaturePad = new SignaturePad(canvas, this.formView.config);
          if(this.formView.penColorInputView.fieldView.element) {
            this.formView.penColorInputView.fieldView.element.addEventListener('input', (event) => {
              this.signaturePad.penColor = event.target.value;
            });
          }
          if(this.formView.sizeInputView.fieldView.element) {
            this.formView.sizeInputView.fieldView.element.addEventListener('input', (event) => {
              this.signaturePad.maxWidth = event.target.value;
            });
          }
        }
      });

      return buttonView;
    });

  }

  _createFormView() {
    const editor = this.editor;
    const formView = new FormView(editor);

    // On submit send the user data to the writer, then hide the form view.
    this.listenTo(formView, 'submit', () => {
      editor.execute('InsertSignatureCommand', this.signaturePad, this.formView.config);
      this._hideUI();
    });

    // Hide the form view after clicking the "Cancel" button.
    this.listenTo(formView, 'cancel', () => {
      this._hideUI();
    });

    // Hide the form view when clicking outside the balloon.
    clickOutsideHandler({
      emitter: formView,
      activator: () => this._balloon.visibleView === formView,
      contextElements: [this._balloon.view.element],
      callback: () => this._hideUI()
    });

    return formView;
  }

  _hideUI() {
    this.formView.element.reset();
    this._balloon.remove(this.formView);
    // Clear signature.
    if(this.signaturePad) {
      this.signaturePad.clear();
      this.signaturePad.off();
    }
  }

  _showUI() {
    this._balloon.add({
      view: this.formView,
      position: this._getBalloonPositionData(),
    });
  }

  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;

    // Set a target position by converting view selection range to DOM.
    target = () => view.domConverter.viewRangeToDom(
      viewDocument.selection.getFirstRange()
    );

    return {
      target
    };
  }

}
