import { Plugin } from 'ckeditor5/src/core';
import SignatureEditing from './SignatureEditing';
import SignatureUI from './SignatureUI';

/**
 * The Signature suggestion plugin.
 *
 * @internal
 */
export default class Signature extends Plugin {
  static get requires() {
    return [SignatureEditing, SignatureUI];
  }
  /**
   * @inheritdoc
   */
  static get pluginName() {
    return "Signature";
  }

}
