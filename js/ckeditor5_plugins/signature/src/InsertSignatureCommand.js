/**
 * @file defines InsertBootstrapIconsCommand, which is executed when the icon
 * toolbar button is pressed.
 */
// cSpell:ignore bootstrapicons

import { Command } from 'ckeditor5/src/core';

export default class InsertSignatureCommand extends Command {
  execute(signaturePad, config) {
    const { model } = this.editor;
    config.svg = signaturePad.toSVG();
    if(config.file_directory != ''){
      fetch(config.url, {
        method: "POST",
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(config)
      })
        .then(response => response.json())
        .then(data => {
          if(data.url){
            model.change((writer) => {
              const attributes = {
                class: ['signature'],
                src: data.url
              };
              // Insert <span class="signature">*</span> at the current selection position
              // in a way that will result in creating a valid model structure.
              model.insertContent(createSvgImg(writer, this.editor.data, attributes));
            });
          }
        })
        .catch(error => {
          console.error('Error:', error);
        });
    } else {
      model.change((writer) => {
        // Insert <span class="signature">*</span> at the current selection position
        // in a way that will result in creating a valid model structure.
        model.insertContent(insertSvgImg(writer, this.editor.data, config.svg));
      });
    }

  }

}

function createSvgImg(writer,data, attributes) {
  // Create instances of the three elements registered with the editor in
  // SignatureEditing.js.
  const svgImg = writer.createElement('signature');
  const currentDate = new Date();
  const year = currentDate.getFullYear();
  const month = currentDate.getMonth() + 1;
  const day = currentDate.getDate();
  const hours = currentDate.getHours();
  const minutes = currentDate.getMinutes();

  let img = `<img src="${attributes.src}" alt="Signature ${day}/${month}/${year} - ${hours}:${minutes}" data-align="left"/>`;
  const viewFragment = data.processor.toView(img);
  const modelFragment = data.toModel(viewFragment);

  writer.append(modelFragment,svgImg);

  // Return the element to be added to the editor.
  return svgImg;
}

function insertSvgImg(writer, data, svg) {
  // Create instances of the three elements registered with the editor in
  // SignatureEditing.js.
  const svgImg = writer.createElement('signature');
  const viewFragment = data.processor.toView(svg);
  const modelFragment = data.toModel(viewFragment);
  writer.append(modelFragment,svgImg);
  // Return the element to be added to the editor.
  return svgImg;
}
