<?php

namespace Drupal\sign_widget\Controller;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Sign widget routes.
 */
final class SignWidgetController extends ControllerBase {

  /**
   * The controller constructor.
   *
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   *   File repository service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   Url generator service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   */
  public function __construct(
    private FileRepositoryInterface $fileRepository,
    private FileUrlGeneratorInterface $fileUrlGenerator,
    private FileSystemInterface $fileSystem,
    private Token $token,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('file.repository'),
      $container->get('file_url_generator'),
      $container->get('file_system'),
      $container->get('token'),
    );
  }

  /**
   * Builds the response.
   */
  public function save(Request $request) {
    $content = Json::decode($request->getContent());
    $svgData = $content['svg'];
    $destination = trim($content['file_directory'], '/');
    // Replace tokens. As the tokens might contain HTML we convert it to plain
    // text.
    $destination = PlainTextOutput::renderFromHtml($this->token->replace($destination, []));
    $destination = 'public://' . $destination . '/';
    $filename = date('ymd') . '_' . rand(1000, 9999) . '.svg';
    if ($this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
      // Save the SVG data to a file.
      $file_path = $destination . $filename;
      $file = $this->fileRepository->writeData($svgData, $file_path, FileExists::Replace);
      $uri = $file->getFileUri();
      $url = $this->fileUrlGenerator->generate($uri)->toString();
      return new JsonResponse(['url' => $url]);
    }
    else {
      // Handle the case where the file upload fails.
      return new JsonResponse(['error' => 'File upload failed.'], 400);
    }
  }

}
