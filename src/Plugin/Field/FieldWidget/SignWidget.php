<?php

namespace Drupal\sign_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Utility\Token;
use Drupal\file\Entity\File;
use Drupal\file\FileRepositoryInterface;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'image_image' widget.
 */
#[FieldWidget(
  id: 'sign_widget',
  label: new TranslatableMarkup('Sign'),
  field_types: ['image'],
)]
class SignWidget extends ImageWidget {

  /**
   * Constructs a new LinkFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param mixed $settings
   *   The formatter settings.
   * @param mixed $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Render\ElementInfoManagerInterface $element_info
   *   The element info manager service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   * @param \Drupal\file\FileRepositoryInterface $fileRepository
   *   File repository service.
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ElementInfoManagerInterface $element_info, ImageFactory $image_factory, protected EntityRepositoryInterface $entityRepository, protected FileSystemInterface $fileSystem, protected FileRepositoryInterface $fileRepository, protected Token $token, protected AccountInterface $currentUser, protected ConfigFactoryInterface $configFactory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $element_info, $image_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('element_info'),
      $container->get('image.factory'),
      $container->get('entity.repository'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'dotSize' => 1,
      'minWidth' => 0.5,
      'maxWidth' => 2.5,
      'backgroundColor' => '',
      'penColor' => '#000000',
      'velocityFilterWeight' => 0.7,
      'signTool' => FALSE,
      'show_remove_btn' => FALSE,
      'local' => FALSE,
      'canvasWidth' => 288,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['dotSize'] = [
      '#type' => 'number',
      '#title' => $this->t('Dot Size'),
      '#default_value' => $this->getSetting('dotSize'),
      '#required' => TRUE,
      '#description' => $this->t('Radius of a single dot.'),
    ];

    $elements['minWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#title' => $this->t('Minimum Width'),
      '#default_value' => $this->getSetting('minWidth'),
      '#required' => TRUE,
      '#description' => $this->t('Minimum width of a line'),
    ];

    $elements['maxWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#title' => $this->t('Maximum Width'),
      '#default_value' => $this->getSetting('maxWidth'),
      '#required' => TRUE,
      '#description' => $this->t('Maximum width of a line.'),
    ];

    $elements['backgroundColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Background Color'),
      '#default_value' => $this->getSetting('backgroundColor'),
      '#description' => $this->t('Color used to clear the background.'),
    ];

    $elements['penColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Pen Color'),
      '#default_value' => $this->getSetting('penColor'),
      '#required' => TRUE,
      '#description' => $this->t('Color used to draw the lines.'),
    ];

    $elements['velocityFilterWeight'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#title' => $this->t('Velocity Filter Weight'),
      '#default_value' => $this->getSetting('velocityFilterWeight'),
      '#required' => TRUE,
      '#description' => $this->t('Weight used to modify new velocity based on the previous velocity.'),
    ];
    $elements['canvasWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Canvas Width'),
      '#default_value' => $this->getSetting('canvasWidth'),
      '#description' => $this->t('Width of the canvas.'),
    ];
    $elements['signTool'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show toolbox'),
      '#default_value' => $this->getSetting('signTool'),
      '#description' => $this->t('Show toolbox to modify dot size and color pen.'),
    ];
    $elements['show_remove_btn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show remove button'),
      '#default_value' => $this->getSetting('show_remove_btn'),
      '#description' => $this->t('Show remove button in edit.'),
    ];
    $elements['local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use local library'),
      '#default_value' => $this->getSetting('local'),
      '#description' => $this->t('Dont use CDN, You can download to <a href="%download">/libraries/signature_pad/docs/js/signature_pad.umd.js</a>.', ['%download' => 'https://github.com/szimek/signature_pad/releases/tag/v4.1.6']),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if (!empty($this->getSetting('dotSize')) && !empty($this->getSetting('minWidth')) && !empty($this->getSetting('maxWidth'))) {
      $summary[] = $this->t('Size :  (%maxWidth x %minWidth)%size', [
        '%size' => $this->getSetting('dotSize'),
        '%minWidth' => $this->getSetting('minWidth'),
        '%maxWidth' => $this->getSetting('maxWidth'),
      ]);
    }
    if (!empty($this->getSetting('backgroundColor'))) {
      $summary[] = $this->t('Background Color : %backgroundColor', ['%backgroundColor' => $this->getSetting('backgroundColor')]);
    }
    if (!empty($this->getSetting('penColor'))) {
      $summary[] = $this->t('Pen Color : %penColor', ['%penColor' => $this->getSetting('penColor')]);
    }
    if (!empty($this->getSetting('velocityFilterWeight'))) {
      $summary[] = $this->t('Velocity Filter Weight : %velocityFilterWeight', ['%velocityFilterWeight' => $this->getSetting('velocityFilterWeight')]);
    }
    if (!empty($this->getSetting('signTool'))) {
      $summary[] = $this->t('Show toolbox');
    }
    if (!empty($this->getSetting('show_remove_btn'))) {
      $summary[] = $this->t('Show remove button');
    }
    if (!empty($this->getSetting('canvasWidth'))) {
      $summary[] = $this->t('Canvas Width : %canvasWidth', ['%canvasWidth' => $this->getSetting('canvasWidth')]);
    }
    if (!empty($this->getSetting('show_remove_btn'))) {
      $summary[] = $this->t('Reset button: @enabled', ['@enabled' => $this->getSetting('show_remove_btn') ? $this->t('Enabled') : $this->t('Disabled')]);
    }

    return $summary;
  }

  /**
   * Overrides FileWidget::formMultipleElements().
   *
   * Special handling for draggable multiple widgets and 'add more' button.
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    switch ($cardinality) {
      case FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED:
        $max = count($items);
        break;

      default:
        $max = $cardinality - 1;
        break;
    }

    $elements = [
      '#type' => 'container',
      '#attributes' => ['class' => ['row']],
    ];
    $element = [];

    // Add an element for every existing item.
    foreach (range(0, $max) as $delta) {
      $elements[$delta] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['esign_container', 'col-sm']],
        '#weight' => $delta,
      ] + $this->formSingleElement($items, $delta, $element, $form, $form_state);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove button doesn't work it must rewrite #submit class.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field_settings = $this->getFieldSettings();

    $values = $items->getValue();
    $max_resolution = $field_settings["max_resolution"];
    $min_resolution = $field_settings["min_resolution"];
    $width = $field_settings["default_image"]["width"] ?? $this->settings['canvasWidth'] ?? 288;
    $height = $field_settings["default_image"]["height"] ?? 180;
    if (!empty($max_resolution)) {
      [$width, $height] = explode('x', $max_resolution);
    }
    if (!empty($min_resolution)) {
      [$width, $height] = explode('x', $min_resolution);
    }
    $settings = $field_settings["default_image"];
    if (!empty($settings['width'])) {
      $width = $settings['width'];
    }
    if (!empty($settings['height'])) {
      $height = $settings['height'];
    }
    if (empty($element['#upload_validators'])) {
      $element['#upload_validators'] = FALSE;
    }
    $field_name = $this->fieldDefinition->getName();
    $id = Html::getUniqueId($field_name);
    $fid = '';
    if (!empty($values[$delta])) {
      $fid = $values[$delta]['target_id'];
      $file = File::load($fid);
    }
    if (empty($file) && !empty($settings["uuid"])) {
      $file = $this->entityRepository->loadEntityByUuid('file', $settings["uuid"]);
    }
    $imgSrc = !empty($file) ? $file->createFileUrl() : '';
    $whiteBg = ['#ffffff', '#FFFFFF', '#000000'];
    if (in_array($this->settings['backgroundColor'], $whiteBg)) {
      $this->settings['backgroundColor'] = 'rgba(255, 255, 255, 0)';
    }
    $attributes = new Attribute([
      'id' => $id,
      'width' => $width,
      'height' => $height,
      'class' => ["signature-pad", "border"],
    ]);
    foreach ($this->settings as $attribute => $settingValue) {
      $attributes['data-' . strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $attribute))] = $settingValue;
    }
    $sign_pad = [
      '#theme' => 'sign',
      '#attributes' => $attributes,
      '#label' => $this->fieldDefinition->getLabel(),
      '#canvas_id' => $id,
      '#sign_src' => $imgSrc,
      '#height' => $height,
      '#width' => $width,
      '#settings' => $this->settings,
    ];

    $element['#attached']['drupalSettings']['sign_pad'][$id] = $this->settings;

    $element += [
      '#type' => 'hidden',
      '#required' => FALSE,
      '#attributes' => [
        'id' => $id . '-sign',
        'class' => ['signature-storage'],
      ],
      '#title_field' => $field_settings['title_field'],
      '#title_field_required' => $field_settings['title_field_required'],
    ];

    $element['#attached']['library'][] = 'sign_widget/sign_widget';

    $file_extensions = $this->fieldDefinition->getSetting("file_extensions");
    $elements = [
      '#title' => $this->fieldDefinition->getLabel(),
      '#title_display' => 'before',
      'value' => $element,
      'canvas' => $sign_pad,
      '#field_parents' => $element['#field_parents'],
      '#upload_validators' => [
        'file_validate_extensions' => [$file_extensions],
      ],
      '#required' => FALSE,
    ];

    // Add the additional alt and title fields.
    if ($field_settings['alt_field']) {
      $elements['alt'] = [
        '#title' => $this->t('Alternative text'),
        '#type' => 'textfield',
        '#default_value' => !empty($values[$delta]['alt']) ? $values[$delta]['alt'] : '',
        '#description' => $this->t('Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.'),
        '#maxlength' => 512,
        '#required' => $field_settings['alt_field_required'],
      ];
    }
    if ($field_settings['title_field']) {
      $elements['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#default_value' => !empty($values[$delta]['title']) ? $values[$delta]['title'] : '',
        '#description' => $this->t('The title is used as a tool tip when the user hovers the mouse over the image.'),
        '#maxlength' => 1024,
        '#required' => $field_settings['title_field_required'],
      ];
    }

    // In case modify.
    if (!empty($fid)) {
      $file = File::load($fid);
      $elements['value']['#default_value'] = $fid;
      $array_parents = array_merge(
        $element["#field_parents"],
        [$field_name, $delta]
      );
      $parents_prefix = implode('_', $array_parents);
      // Generate a unique wrapper HTML ID.
      $ajax_settings = [
        'callback' => ['Drupal\file\Element\ManagedFile', 'uploadAjaxCallback'],
        'options' => [
          'query' => [
            'element_parents' => implode('/', array_merge(
              $array_parents, ['widget', $delta]
            )),
          ],
        ],
        'wrapper' => Html::getId('edit-' .
          implode('-', $array_parents)
        ),
        'effect' => 'none',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ];
      $remove_button = boolval($this->getSetting("show_remove_btn"));

      if ($this->currentUser->hasPermission('administer site configuration')) {
        $remove_button = TRUE;
      }
      // Force the progress indicator for the remove btn to be either 'none' or
      // 'throbbed', even if the upload button is using something else.
      $elements['remove_button'] = [
        '#name' => $parents_prefix . '_remove_button',
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#access' => $remove_button,
        '#submit' => [
          'file_managed_file_submit',
          ['Drupal\file\Plugin\Field\FieldWidget\FileWidget' , 'submit'],
        ],
        '#ajax' => $ajax_settings,
      ];
      if ($file) {
        $elements['#files'] = [$fid => $file->setTemporary()];
        $elements['#multiple'] = FALSE;
        $elements['#prefix'] = '';
        $elements['#suffix'] = '';
      }
      $elements['fids'] = [
        '#type' => 'hidden',
        '#value' => [$fid],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $file_directory = trim($this->fieldDefinition->getSetting("file_directory"), '/');
    $destination = PlainTextOutput::renderFromHtml($this->token->replace($file_directory, []));
    $uri = $this->configFactory->get('system.file')->get('default_scheme') . '://';
    if (!empty($destination)) {
      $uri .= $destination . '/';
      $path = $this->fileSystem->realpath($uri);
      if (!$path) {
        $this->fileSystem->prepareDirectory($uri, FileSystemInterface::CREATE_DIRECTORY);
      }
    }
    if (empty($form["#validated"])) {
      return $values;
    }
    foreach ($values as &$value) {
      if (!empty($value['value']) && empty($values['fids'])) {
        if (is_numeric($value['value'])) {
          $file = File::load($value['value']);
        }
        else {
          $encoded_image = explode(",", $value['value'])[1];
          $encoded_image = str_replace(' ', '+', $encoded_image);
          $decoded_image = base64_decode($encoded_image);
          $filename = date('ymd') . '_' . rand(1000, 9999) . '.png';
          // Saves a file to the specified folder & creates a database entry.
          $file = $this->fileRepository->writeData($decoded_image, $uri . $filename, FileExists::Replace);
        }
        if (!empty($file)) {
          [$value['width'], $value['height']] = getimagesize($file->getFileUri());
          $value['target_id'] = $file->id();
        }
        if (!empty($value['fids'])) {
          unset($value['fids']);
        }
        if (!empty($value['remove_button'])) {
          unset($value['remove_button']);
        }
        unset($value['value']);
      }
    }
    return $values;
  }

}
