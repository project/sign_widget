<?php

namespace Drupal\sign_widget\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefinition;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\editor\EditorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CKEditor 5 Signature plugin.
 */
class Signature extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface, ContainerFactoryPluginInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * Constructs a new Signature instance.
   *
   * {@inheritDoc}
   */
  public function __construct(array $configuration, string $plugin_id, CKEditor5PluginDefinition $plugin_definition, protected LibraryDiscoveryInterface $libraryDiscovery) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('library.discovery'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'dotSize' => 1.0,
      'minWidth' => 0.5,
      'maxWidth' => 2.5,
      'backgroundColor' => '',
      'penColor' => '#000000',
      'velocityFilterWeight' => 0.7,
      'signTool' => FALSE,
      'local' => FALSE,
      'file_directory' => 'inline-images',
      'canvasWidth' => 288,
      'show_remove_btn' => TRUE,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['dotSize'] = [
      '#type' => 'number',
      '#title' => $this->t('Dot Size'),
      '#default_value' => $this->configuration['dotSize'],
      '#required' => TRUE,
      '#description' => $this->t('Radius of a single dot.'),
    ];

    $form['minWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#title' => $this->t('Minimum Width'),
      '#default_value' => $this->configuration['minWidth'],
      '#required' => TRUE,
      '#description' => $this->t('Minimum width of a line'),
    ];

    $form['maxWidth'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#title' => $this->t('Maximum Width'),
      '#default_value' => $this->configuration['maxWidth'],
      '#required' => TRUE,
      '#description' => $this->t('Maximum width of a line.'),
    ];

    $form['backgroundColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Background Color'),
      '#default_value' => $this->configuration['backgroundColor'],
      '#description' => $this->t('Color used to clear the background.'),
    ];

    $form['penColor'] = [
      '#type' => 'color',
      '#title' => $this->t('Pen Color'),
      '#default_value' => $this->configuration['penColor'],
      '#required' => TRUE,
      '#description' => $this->t('Color used to draw the lines.'),
    ];

    $form['velocityFilterWeight'] = [
      '#type' => 'number',
      '#step' => '.1',
      '#title' => $this->t('Velocity Filter Weight'),
      '#default_value' => $this->configuration['velocityFilterWeight'],
      '#required' => TRUE,
      '#description' => $this->t('Weight used to modify new velocity based on the previous velocity.'),
    ];
    $form['canvasWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Canvas Width'),
      '#default_value' => $this->configuration['canvasWidth'],
      '#required' => TRUE,
      '#description' => $this->t('Width of the canvas.'),
    ];
    $form['show_remove_btn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Reset Button'),
      '#default_value' => $this->configuration['show_remove_btn'],
      '#description' => $this->t('Enable the reset button.'),
    ];
    $form['file_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File directory'),
      '#default_value' => $this->configuration['file_directory'],
      '#description' => $this->t('Optional subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
    ];
    $form['signTool'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Toolbox'),
      '#default_value' => $this->configuration['signTool'],
      '#description' => $this->t('Show toolbox to modify dot size and color pen.'),
    ];
    $form['local'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use local library'),
      '#default_value' => $this->configuration['local'] ?? FALSE,
      '#description' => $this->t("Don't use CDN, You can download to <a href='@download'>/libraries/signature_pad/docs/js/signature_pad.umd.js</a>.", ['@download' => 'https://github.com/szimek/signature_pad/releases/tag/v4.1.6']),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['dotSize'] = $form_state->getValue('dotSize') ?? 1;
    $this->configuration['minWidth'] = $form_state->getValue('minWidth') ?? 0.5;
    $this->configuration['maxWidth'] = $form_state->getValue('maxWidth') ?? 2.5;
    $this->configuration['backgroundColor'] = $form_state->getValue('backgroundColor') ?? '#000000';
    $this->configuration['canvasWidth'] = $form_state->getValue('canvasWidth') ?? 300;
    $this->configuration['show_remove_btn'] = $form_state->getValue('show_remove_btn') ?? FALSE;
    $this->configuration['penColor'] = $form_state->getValue('penColor') ?? '';
    $this->configuration['velocityFilterWeight'] = $form_state->getValue('velocityFilterWeight') ?? 0.7;
    $this->configuration['signTool'] = $form_state->getValue('signTool') ?? FALSE;
    $this->configuration['local'] = $form_state->getValue('local') ?? FALSE;
    $this->configuration['file_directory'] = $form_state->getValue('file_directory') ?? 'inline-images';
  }

  /**
   * {@inheritdoc}
   *
   * Get options values in editor config.
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $config = $this->configuration;
    $lib = 'signature_pad';
    if (!empty($config['local'])) {
      $lib .= '.local';
    }
    $url = Url::fromRoute('sign_widget.save');
    $config['url'] = $url->toString();
    $library_info = $this->libraryDiscovery->getLibraryByName('sign_widget', $lib);
    $config['cdn'] = $library_info["js"][0]["data"];
    if (in_array($config['backgroundColor'], ['#000000', '#000'])) {
      $config['backgroundColor'] = '#fff';
    }
    return [
      'signature' => $config,
    ];
  }

}
