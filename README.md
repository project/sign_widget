# Signature pad widget

Signature pad an electronic-signing script, this Image Widget allows you to
sign with HTML5 canvas based on field image.

- Your signature will save as an image.
- Drawing over an image with image default

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sign_widget).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sign_widget).

## Requirements

This module requires no modules outside of Drupal core.
Create a field image. in form display select "Sign"

## Installation

For Field API, you can change the field options in an associative array:
dotSize (default: 1)
minWidth (default: 0.5)
maxWidth (default: 2.5)
backgroundColor (default: 'rgba(0,0,0,0)')
canvasWidth (default: 288)
show_remove_btn
penColor (default: 'rgba(0,0,0,1)')
velocityFilterWeight (default: 0.7)


## Configuration

The module currently provides no configuration options.
If you don't want to show the double sign in edit mode,
you can choose a white background.


## Maintainers

- NGUYEN Bao - [lazzyvn](https://www.drupal.org/u/lazzyvn)

#### Support and Appreciation
If you find the Sign pad module valuable and would like to show
your appreciation, consider [buying a cup of coffee ☕](https://www.buymeacoffee.com/nbao) for the developer.
